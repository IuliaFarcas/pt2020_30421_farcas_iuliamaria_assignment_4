package view;

import controller.IRestaurantProcessing;
import controller.SerializationOP;
import model.BaseProduct;
import model.MenuItem;
import model.Order;
import model.Restaurant;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class AdministratorGUI extends JFrame implements IRestaurantProcessing {

    private Restaurant restaurant;
    private CompositeItemGUI compositeItemGUI;

    private JLabel title;
    private JButton back;
    private JButton addmenu;
    private JButton editmenu;
    private JButton deletemenu;
    private JButton viewmenu;

    private JLabel namelabel;
    private JTextField namefield;
    private JLabel pricelabel;
    private JTextField pricefield;

    public AdministratorGUI(UI ui, Restaurant restaurant, CompositeItemGUI compositeItemGUI)
    {
        this.restaurant = restaurant;
        this.compositeItemGUI = compositeItemGUI;

        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setBounds(500, 150, 900, 700);
        this.getContentPane().setLayout(null);

        title = new JLabel("Administrator window");
        title.setBounds(375, 20, 450, 50);
        getContentPane().add(title);

        namelabel = new JLabel("Name");
        namelabel.setBounds(50,150,50,40);
        getContentPane().add(namelabel);

        pricelabel = new JLabel("Price");
        pricelabel.setBounds(50,200,50,40);
        getContentPane().add(pricelabel);

        namefield = new JTextField();
        namefield.setBounds(100,150,100,30);
        getContentPane().add(namefield);

        pricefield = new JTextField();
        pricefield.setBounds(100,200,100,30);
        getContentPane().add(pricefield);

        back = new JButton("Back");
        back.setBounds(750, 550, 100, 50);
        back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                setVisible(false);
                ui.setVisible(true);

            }
        });
        getContentPane().add(back);

        addmenu = new JButton("Add menu item");
        addmenu.setBounds(50, 550, 125, 50);
        getContentPane().add(addmenu);
        addmenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String itemName = namefield.getText();
                String itemPrice = pricefield.getText();
                if (itemPrice.equals("")) {
                    System.out.println("Composite !");
                    compositeItemGUI.setVisible(true);
                    compositeItemGUI.fillBox();
                    restaurant.setCompName(itemName);
                } else {

                    int itemP = 0;
                    try {
                        itemP = Integer.parseInt(itemPrice);
                    } catch (Exception e1) {
                        JOptionPane.showMessageDialog(null, "Invalid price !");
                    }

                    BaseProduct newBaseProduct = new BaseProduct(itemName, itemP);
                    addMenu(newBaseProduct);
                    SerializationOP.write(restaurant);
                    JOptionPane.showMessageDialog(null, "Item added successfully !");
                }
            }
        });

        editmenu = new JButton("Edit menu item");
        editmenu.setBounds(200, 550, 125, 50);
        getContentPane().add(editmenu);
        editmenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String itemName = namefield.getText();
                String itemPrice = pricefield.getText();
                int price = 0;
                try{
                    price = Integer.parseInt(itemPrice);
                    MenuItem old = findByName(itemName);
                    BaseProduct newprod = new BaseProduct(itemName, price);
                    if(old.getClass() == newprod.getClass())
                    {
                        old.setPrice(price);
                        JOptionPane.showMessageDialog(null, "Item edit successfully!");
                        editMenu(old);
                        SerializationOP.write(restaurant);
                    }
                    else JOptionPane.showMessageDialog(null, "Can't edit the menu!");
                }
                catch (Exception e)
                {
                    JOptionPane.showMessageDialog(null, "Invalid price!");
                }
            }
        });

        deletemenu = new JButton("Delete menu item");
        deletemenu.setBounds(350, 550, 125, 50);
        getContentPane().add(deletemenu);
        deletemenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String itemName = namefield.getText();
                restaurant.deleteMenu(findByName(itemName));
                SerializationOP.write(restaurant);

                JOptionPane.showMessageDialog(null, "Successfully deleted the item!");

            }
        });

        viewmenu = new JButton("Show menu items");
        viewmenu.setBounds(550,550,125,50);
        getContentPane().add(viewmenu);
        viewmenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String[] col = {"Name", "Price"};
                ArrayList<MenuItem> list = restaurant.getMenu();
                DefaultTableModel model = new DefaultTableModel();
                model.setColumnIdentifiers(col);
                Object[] objects = new Object[2];
                for(MenuItem m : list)
                {
                    objects[0] = m.getName();
                    objects[1] = m.getPrice();
                    model.addRow(objects);
                }
                JTable table = new JTable(model);
                JScrollPane scrollPane = new JScrollPane();
                scrollPane.setBounds(250,100,600,400);
                scrollPane.setViewportView(table);
                getContentPane().add(scrollPane);
            }
        });

    }


    public void addOrder(Order order, ArrayList<MenuItem> menuItems)
    {
        System.out.println("Not allowed to create orders.");
    }
    public int getFinalPrice(Order order)
    {
        System.out.println("Not allowed to process the final price.");
        return 0;
    }
    public void bill(String toPrint)
    {
        System.out.println("Not allowed to generate bills.");
    }

    public void addMenu(MenuItem menuItem)
    {
        restaurant.addMenu(menuItem);
    }

    public void editMenu(MenuItem menuItem)
    {
        restaurant.editMenu(menuItem);
    }

    @Override
    public void deleteMenu(MenuItem menuItem) {
        restaurant.deleteMenu(menuItem);
    }

    public MenuItem findByName(String name)
    {
        ArrayList<MenuItem> list = restaurant.getMenu();
        for(MenuItem m : list )
        {
            if(m.getName().equals(name))
            {
                return m;
            }
        }
        return null;
    }

}
