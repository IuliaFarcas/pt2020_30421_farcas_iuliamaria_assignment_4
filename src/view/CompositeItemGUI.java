package view;

import controller.SerializationOP;
import model.CompositeProduct;
import model.MenuItem;
import model.Restaurant;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class CompositeItemGUI extends JFrame
{
    private Restaurant restaurant;
    private JLabel title;
    private ArrayList<MenuItem> compositeItem;
    private JComboBox<MenuItem> comboBox;
    private JButton additem;
    private JButton finished;
    private JTextArea addedItems;

    public CompositeItemGUI(Restaurant restaurant)
    {
        compositeItem = new ArrayList<MenuItem>();
        this.restaurant = restaurant;
        this.setDefaultCloseOperation(HIDE_ON_CLOSE);
        this.setBounds(500,150,900,700);
        this.getContentPane().setLayout(null);

        title = new JLabel("Composite items operations");
        title.setBounds(20,50,450,50);
        getContentPane().add(title);

        comboBox = new JComboBox<MenuItem>();
        comboBox.setBounds(300,50,450,50);
        getContentPane().add(comboBox);

        addedItems = new JTextArea();
        addedItems.setBounds(400,150,400,300);
        getContentPane().add(addedItems);

        additem = new JButton("Add Item");
        finished = new JButton("Finish");

        additem.setBounds(50,550,125,50);
        getContentPane().add(additem);
        additem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                MenuItem item = (MenuItem)comboBox.getSelectedItem();
                compositeItem.add(item);
                addedItems.append(item.toString() + "\n");
            }
        });

        finished.setBounds(250,550,125,50);
        getContentPane().add(finished);
        finished.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                restaurant.setCompositeProd(compositeItem);
                ArrayList<MenuItem> item = restaurant.getCompositeProd();
                String name = restaurant.getCompName();

                CompositeProduct compositeProduct = new CompositeProduct(name, item);
                int price = compositeProduct.computePrice();
                compositeProduct.setPrice(price);

                restaurant.addMenu(compositeProduct);
                //serialize restaurant
                SerializationOP.write(restaurant);
            }
        });
    }

    public void fillBox()
    {
        ArrayList<MenuItem> list = restaurant.getMenu();

        for(MenuItem m : list)
        {
            comboBox.addItem(m);
        }

    }
}
