package view;

import javax.swing.*;
import java.awt.event.ActionListener;

public class UI extends JFrame {
    private JLabel title;
    private JButton admin;
    private JButton waiter;
    private JButton chef;

    public UI()
    {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setBounds(500, 150, 620, 350);
        this.getContentPane().setLayout(null);

        title = new JLabel("Main menu.");
        title.setBounds(275,40,450,50);
        getContentPane().add(title);

        admin = new JButton("Administrator");
        admin.setBounds(20,200,150,50);
        getContentPane().add(admin);

        waiter = new JButton("Waiter");
        waiter.setBounds(275,200,100,50);
        getContentPane().add(waiter);

        chef = new JButton("Chef");
        chef.setBounds(480,200,100,50);
        getContentPane().add(chef);
    }

    public void adminAL(ActionListener actionListener)
    {
        admin.addActionListener(actionListener);
    }
    public void waiterAL(ActionListener actionListener)
    {
        waiter.addActionListener(actionListener);
    }
    public void chefAL(ActionListener actionListener)
    {
        chef.addActionListener(actionListener);
    }
}
