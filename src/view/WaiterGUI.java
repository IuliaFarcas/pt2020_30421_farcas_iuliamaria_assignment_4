package view;

import controller.IRestaurantProcessing;
import model.Date;
import model.MenuItem;
import model.Order;
import model.Restaurant;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Map;

public class WaiterGUI extends JFrame implements IRestaurantProcessing {

    private Restaurant restaurant;

    private JLabel title;
    private JButton back;
    private JButton addorder;
    private JButton addmenuItem;
    private JButton vieworders;
    private JButton generatebill;
    private JButton fillmenu;

    ArrayList<MenuItem> items = new ArrayList<MenuItem>();
    ArrayList<Order> orders = new ArrayList<Order>();

    private JLabel orderlabel;
    private JLabel tablelabel;
    private JLabel datelabel;
    private JTextField orderfield;
    private JTextField tablefield;
    private JTextField datefield;
    private JLabel chosenlabel;
    private JTextArea chosentext;

    private JComboBox<MenuItem> comboBox;

    int id = 0;

    public WaiterGUI(UI ui, Restaurant restaurant)
    {
        this.restaurant = restaurant;
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setBounds(500, 150, 1200, 700);
        this.getContentPane().setLayout(null);

        title = new JLabel("Waiter window");
        title.setBounds(550, 20, 450, 50);
        getContentPane().add(title);

        orderlabel = new JLabel("OrderID:");
        orderlabel.setBounds(50, 110, 80, 30);
        getContentPane().add(orderlabel);
        orderfield = new JTextField();
        orderfield.setBounds(105, 110, 50, 30);
        getContentPane().add(orderfield);

        tablelabel = new JLabel("Table:");
        tablelabel.setBounds(50, 150, 50, 30);
        getContentPane().add(tablelabel);
        tablefield = new JTextField();
        tablefield.setBounds(105, 150, 50, 30);
        getContentPane().add(tablefield);

        datelabel = new JLabel("Date:");
        datelabel.setBounds(50, 200, 50, 30);
        getContentPane().add(datelabel);
        datefield = new JTextField();
        datefield.setBounds(105, 200, 80, 30);
        getContentPane().add(datefield);

        comboBox = new JComboBox<MenuItem>();
        comboBox.setBounds(50,250,150,50);
        getContentPane().add(comboBox);

        chosenlabel = new JLabel("Chosen items");
        chosenlabel.setBounds(230, 130, 100, 20);
        getContentPane().add(chosenlabel);

        chosentext = new JTextArea();
        chosentext.setBounds(230, 150, 100, 200);
        getContentPane().add(chosentext);

        fillMenu();

        back = new JButton("Back");
        back.setBounds(750, 550, 100, 50);
        back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                setVisible(false);
                ui.setVisible(true);

            }
        });
        getContentPane().add(back);

        addmenuItem = new JButton("Add");
        addmenuItem.setBounds(50, 320, 170, 30);
        getContentPane().add(addmenuItem);
        addmenuItem.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                MenuItem myItem = (MenuItem) comboBox.getSelectedItem();
                chosentext.append(myItem.toString()+"\n");
                items.add(myItem);
                System.out.println(myItem.getName());

            }
        });

        fillmenu = new JButton("Refresh menu");
        fillmenu.setBounds(50, 370, 170, 30);
        getContentPane().add(fillmenu);
        fillmenu.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                fillMenu();
            }
        });

        addorder = new JButton("Create Order");
        addorder.setBounds(50, 550, 150, 50);
        getContentPane().add(addorder);
        addorder.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try
                {
                    int orderID = genID();
                    int table = Integer.parseInt(tablefield.getText());
                    String date = datefield.getText();
                    if(date.matches("[0-9][0-9].[0-9][0-9].[0-9][0-9][0-9][0-9]")==false)
                    {
                        System.out.println("Wrong date input");
                    }
                    System.out.println("OrderID: " + orderID + ", table: " + table + ", date: " + date);
                    String[] s = date.split("\\.");
                    int d = Integer.parseInt(s[0]);
                    int m = Integer.parseInt(s[1]);
                    int y = Integer.parseInt(s[2]);

                    Date data = new Date(d,m,y);
                    Order order = new Order(orderID, data, table);

                    ArrayList<MenuItem> list = new ArrayList<MenuItem>();
                    for(MenuItem mi: items)
                    {
                        list.add(mi);
                    }
                    int size = items.size();
                    if(size < 0)
                    {
                        JOptionPane.showMessageDialog(null, "Client did't order yet, empty order!");
                    }
                    else
                    {
                        addOrder(order, list);
                        orders.add(order);
                        restaurant.addToOrders(order);
                        items.removeAll(items);
                        chosentext.setText("");
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(null, "Wrong input");
                }
            }
        });

        vieworders = new JButton("Show Orders");
        vieworders.setBounds(550, 550, 150, 50);
        getContentPane().add(vieworders);
        vieworders.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Map<Order, ArrayList<MenuItem>> map = restaurant.getMap();
                String[] col = { "OrderID", "Date", "Table", "OrderedItems" };

                DefaultTableModel model = new DefaultTableModel();
                model.setColumnIdentifiers(col);
                Object[] object = new Object[4];

                for(Order o: orders)
                {
                    object[0] = o.getOrderID();
                    String date = o.getDate().getDay() + "." + o.getDate().getMonth() + "." + o.getDate().getYear();
                    object[1] = date;
                    object[2] = o.getTable();

                    ArrayList<MenuItem> list = map.get(o);
                    StringBuilder s = new StringBuilder();
                    for(MenuItem m : list)
                    {
                        s.append(m.toString() + " , ");
                    }
                    s.deleteCharAt(s.length() - 1);
                    s.deleteCharAt(s.length() - 1);
                    object[3] = s;
                    model.addRow(object);
                }
                JTable table = new JTable(model);
                JScrollPane scrollPane = new JScrollPane();
                scrollPane.setBounds(350, 100, 800, 400);
                scrollPane.setViewportView(table);
                scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
                scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
                getContentPane().add(scrollPane);
            }
        });

        generatebill = new JButton("Generate Bill !");
        generatebill.setBounds(300, 550, 200, 50);
        getContentPane().add(generatebill);
        generatebill.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int orderID = Integer.parseInt(orderfield.getText());
                Order order = findOrder(orderID);
                if (order == null)
                    JOptionPane.showMessageDialog(null, "Order not found!");
                else
                {
                    StringBuilder s = new StringBuilder();
                    s.append("Order ID: " + orderID + "\n");
                    s.append("Date: " + order.getDate().getDay() + "."
                            + order.getDate().getMonth() + "." + order.getDate().getYear() + "\n");
                    s.append("Table: " + order.getTable() + "\n");
                    s.append("Ordered Items: " + "\n");

                    Map<Order, ArrayList<MenuItem>> map = restaurant.getMap();
                    ArrayList<MenuItem> list = map.get(order);
                    for(MenuItem m : list)
                    {
                        s.append(m.toString() + "\n");
                    }
                    s.append("Total price: " + getFinalPrice(order));
                    bill(s.toString());
                    JOptionPane.showMessageDialog(null, "Bill generated successfully!");
                }
            }
        });
        
        
    }

    public int genID()
    {
        return ++id;
    }


    public void fillMenu()
    {
        comboBox.removeAllItems();
        ArrayList<MenuItem> list = restaurant.getMenu();
        for(MenuItem m : list)
        {
            comboBox.addItem(m);
        }
    }

    public Order findOrder(int id)
    {
        //Order order = null;
        for(Order o : orders)
        {
            if(o.getOrderID() == id)
                return o;
        }
        return null;
    }

    public void addMenu(MenuItem menuItem)
    {
        System.out.println("Not allowed to add new menu.");
    }
    public void editMenu(MenuItem menuItem)
    {
        System.out.println("Not allowed to edit menu.");
    }
    public void deleteMenu(MenuItem menuItem)
    {
        System.out.println("Not allowed to delete menu item.");
    }

    public void addOrder(Order order, ArrayList<MenuItem> menuItems)
    {
        restaurant.addOrder(order, menuItems);
    }
    public int getFinalPrice(Order order)
    {
      return restaurant.getFinalPrice(order);
    }
    public void bill(String toPrint)
    {
        restaurant.bill(toPrint);
    }
}
