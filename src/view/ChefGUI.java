package view;

import model.Restaurant;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

public class ChefGUI extends JFrame implements Observer {

    private JLabel title;
    private JButton back;
    private Restaurant restaurant;

    public ChefGUI(UI ui, Restaurant restaurant)
    {
        this.restaurant = restaurant;
        restaurant.addObserver(this);
        this.setDefaultCloseOperation(HIDE_ON_CLOSE);
        this.setBounds(500, 150, 600, 400);
        this.getContentPane().setLayout(null);

        title = new JLabel("Chef window");
        title.setBounds(250, 50, 450, 50);
        getContentPane().add(title);

        back = new JButton("Back");
        back.setBounds(450, 250, 100, 50);
        back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                setVisible(false);
                ui.setVisible(true);

            }
        });
        getContentPane().add(back);
    }

    public void update(Observable observable, Object object)
    {
        this.setVisible(true);
        System.out.println(object.toString());
        int notif = JOptionPane.showConfirmDialog(null, object, "New Notification", 2);
        if( notif == 0)
        {
            System.out.println("Chef available, will cook!");
            this.setVisible(false);
        }
        else
        {
            System.out.println("Chef is busy, will cook later!");
            this.setVisible(false);
        }
    }

}
