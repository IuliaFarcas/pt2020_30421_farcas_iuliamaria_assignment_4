package controller;

import model.MenuItem;
import model.Order;

import java.util.ArrayList;

public interface IRestaurantProcessing {

    //menu operations
    /**
     * pre: menuItem != null
     * post: list.size(pre) == list.size(post)-1
     * */
    public void addMenu(MenuItem menuItem);
    /**
     * pre: menuItem != null
     * post: menuItem.price(pre)!=menuItem.price(post)
     * */
    public void editMenu(MenuItem menuItem);
    /**
     * pre: menuItem != null
     * post: list.size(post) == list.size(pre)+1
     * */
    public void deleteMenu(MenuItem menuItem);

    //order operations
    /**
     * pre: order!=null, menuItem!=null
     */
    public void addOrder(Order order, ArrayList<MenuItem> menuItems);

    /**
     * pre: order!=null
     */
    public int getFinalPrice(Order order);
    /**
     * pre: toPrint!=null
     */
    public void bill(String toPrint);
}
