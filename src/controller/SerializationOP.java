package controller;

import model.Restaurant;

import java.io.*;

public class SerializationOP {

    public static void write(Restaurant restaurant)
    {
        try
        {
            FileOutputStream output = new FileOutputStream("restaurant.ser");
            ObjectOutputStream out = new ObjectOutputStream(output);
            out.writeObject(restaurant);
            out.close();
            output.close();
            System.out.println("Serialized the data in restaurant.ser");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Restaurant read()
    {
        Restaurant restaurant = null;
        try
        {
            FileInputStream input = new FileInputStream("restaurant.ser");
            ObjectInputStream in = new ObjectInputStream(input);
            restaurant = (Restaurant) in.readObject();
            in.close();
            input.close();
            System.out.println(restaurant);
            return  restaurant;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return restaurant = new Restaurant();
    }
}
