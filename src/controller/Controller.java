package controller;

import model.Restaurant;
import view.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller {

    private AdministratorGUI administratorGUI;
    private CompositeItemGUI compositeItemGUI;
    private WaiterGUI waiterGUI;
    private ChefGUI chefGUI;
    private UI ui;
    private Restaurant restaurant;

    public void initUIButtons()
    {
        ui.adminAL(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ui.setVisible(false);
                administratorGUI.setVisible(true);
            }
        });

        ui.waiterAL(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ui.setVisible(false);
                waiterGUI.setVisible(true);
            }
        });

        ui.chefAL(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ui.setVisible(false);
                chefGUI.setVisible(true);
            }
        });
    }

    public void begin()
    {
        restaurant = SerializationOP.read();
        ui = new UI();
        compositeItemGUI = new CompositeItemGUI(restaurant);
        administratorGUI = new AdministratorGUI(ui, restaurant, compositeItemGUI);
        waiterGUI = new WaiterGUI(ui, restaurant);
        chefGUI = new ChefGUI(ui, restaurant);

        ui.setVisible(true);
        initUIButtons();
    }
}
