package model;

public class Order {

    public int orderID;
    public Date date;
    public int table;

    public Order(int orderID, Date date, int table)
    {
        this.orderID = orderID;
        this.date = date;
        this.table = table;
    }

    public int getOrderID()
    {
        return this.orderID;
    }

    public int getTable()
    {
        return this.table;
    }

    public Date getDate()
    {
        return this.date;
    }

    public int hashCode()
    {
        int hash = 7;
        hash = hash + hash * 1 + orderID * 3 + table * 5 + date.getDay() * 7;
        return hash;
    }
}
