package model;

import java.io.Serializable;

public abstract class MenuItem implements Serializable {

    public String name;
    public int price;

    public abstract int computePrice();

    public int getPrice()
    {
        return this.price;
    }
    public void setPrice(int price)
    {
        this.price = price;
    }
    public String getName()
    {
        return this.name;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public String toString()
    {
        return "Name: " + this.name + ", price: "+ this.price;
    }
}
