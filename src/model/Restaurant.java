package model;

import controller.IRestaurantProcessing;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

public class Restaurant extends Observable implements IRestaurantProcessing, Serializable {

    private ArrayList<MenuItem> menu = new ArrayList<MenuItem>();
    private ArrayList<MenuItem> compositeProd = new ArrayList<MenuItem>();
    private ArrayList<Order> orders = new ArrayList<Order>();
    private String compName;
    private Map<Order, ArrayList<MenuItem>> map;

    public Restaurant()
    {
        this.map = new HashMap<Order, ArrayList<MenuItem>>();
    }

    public ArrayList<MenuItem> getMenu()
    {
        return this.menu;
    }
    public ArrayList<MenuItem> getCompositeProd()
    {
        return this.compositeProd;
    }
    public String getCompName()
    {
        return this.compName;
    }
    public ArrayList<Order> getOrders()
    {
        return this.orders;
    }
    public Map<Order, ArrayList<MenuItem>> getMap()
    {
        return this.map;
    }
    public void setMenu(ArrayList<MenuItem> menu)
    {
        this.menu = menu;
    }
    public  void setCompositeProd(ArrayList<MenuItem> compositeProd)
    {
        this.compositeProd = compositeProd;
    }
    public void setCompName(String compName)
    {
        this.compName = compName;
    }

    public void addToOrders(Order order)
    {
        orders.add(order);
        StringBuilder toWrite = new StringBuilder();
        toWrite.append("New Order!\n");
        toWrite.append("Order ID: " + order.getOrderID() + "\n");
        toWrite.append("Table: " + order.getTable() + "\n");
        toWrite.append("Products: \n");

        Map<Order, ArrayList<MenuItem>> map2 = map;
        ArrayList<MenuItem> menuProducts = map2.get(order);

        for(MenuItem m : menuProducts)
        {
            toWrite.append(m.toString() + "\n");
        }

        setChanged();
        notifyObservers(toWrite.toString());
    }

    public void addOrder(Order order, ArrayList<MenuItem> menuItems)
    {
        assert order != null;
        assert menuItems != null;

        if(order != null && menuItems != null)
        {
            map.put(order, menuItems);
        }
        else{
            System.out.println("ERROR: Null order or menu items");
        }

    }

    public int getFinalPrice(Order order)
    {
        assert order != null;
        int price = 0;
        if(order != null && orders.contains(order) == true)
        {
            ArrayList<MenuItem> items = map.get(order);
            for(MenuItem m : items)
            {
                price = price + m.getPrice();
            }

        }
        return price;
    }

    public void bill(String toPrint)
    {
        assert toPrint != null;
        try
        {
            FileOutputStream file = new FileOutputStream("bill.txt");
            OutputStreamWriter writer = new OutputStreamWriter(file);
            BufferedWriter bufferedWriter = new BufferedWriter(writer);
            bufferedWriter.write(toPrint);
            bufferedWriter.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void addMenu(MenuItem menuItem)
    {
        assert menuItem != null;
        int pre = menu.size();
        if(menuItem != null)
        {
            menu.add(menuItem);
        }
        else
        {
            System.out.println("Error, null menu item");
        }
        int post = menu.size();
        assert pre + 1 == post;
    }

    public void editMenu(MenuItem menuItem)
    {
        assert menuItem != null;
        if(menuItem != null)
        {
            for(MenuItem m : menu)
            {
                if(m.getName() == menuItem.getName())
                {
                    m.setPrice(menuItem.getPrice());
                }
            }
        }
        else{
            System.out.println("Error, null item");
        }
    }

    public  void deleteMenu(MenuItem menuItem)
    {
        assert menuItem != null;
        int pre = menu.size();
        if(menuItem != null)
        {
            menu.remove(menuItem);
        }
        else
        {
            System.out.println("Error, null item");
        }
        int post = menu.size();
        assert pre == post + 1;
    }
}

