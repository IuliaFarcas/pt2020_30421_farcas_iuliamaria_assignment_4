package model;

import java.util.ArrayList;

public class CompositeProduct extends MenuItem {

    private ArrayList<MenuItem> compositeProduct;
    public CompositeProduct(String name, ArrayList<MenuItem> compositeProduct)
    {
        this.name = name;
        this.compositeProduct = compositeProduct;
    }

    public int computePrice()
    {
        int price = 0;
        for(MenuItem m: compositeProduct)
        {
            price = price + m.getPrice();
        }
        return price;
    }
}
