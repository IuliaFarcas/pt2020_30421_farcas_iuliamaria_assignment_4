package model;

public class BaseProduct extends MenuItem {

    public BaseProduct(String name, int price)
    {
        this.name = name;
        this.price = price;
    }

    public int computePrice()
    {
        return getPrice();
    }

}
